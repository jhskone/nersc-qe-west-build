# QE-WEST BUILD on NERSC

## USAGE: 
Login to NERSC Edison machine. 

Clone repo: 
git clone https://gitlab.com/jhskone/nersc-qe-west-build.git

Run build script: 
/bin/bash qe-west-build-nersc.sh


## NOTES: 

Build script has modules, flags and compilers/libs set for Edison. The build
script will copy the modified make.inc to the QE base directory to make build. 
Build currently uses intel/intelmpi compilers and mkl math library.