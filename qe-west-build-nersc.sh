#!/bin/bash
#
# SET QE VERSION TO BUILD
QE_VERSION=6.1.0
# TURN ON/OFF WEST BUILD & SET WEST VERSION
BUILD_WEST=true
WEST_VERSION=3.1.1
#
# SET THE INSTALL DIRECTORY
# IF YOU DO NOT SET THIS THEN
# THE BINARIES WILL BE INSTALLED
# IN THE ESPRESSO /bin FOLDER :
CURR_DIR=`pwd`
INSTALL_DIR=$CURR_DIR/qe-$QE_VERSION
#
# CHECK OUT QE VERSION
git clone -b qe-$QE_VERSION --single-branch --depth 1 https://gitlab.com/QEF/q-e.git qe-$QE_VERSION
cd $CURR_DIR/qe-$QE_VERSION
#
# SET THE MODULES NEEDED TO BUILD:
#
# COMPILER AND MPI LIB MODULES
# USING INTELMPI ON NERSC
COMP_MOD="intel/18.0.1.163 impi/2018.up1"
#COMP_MOD=" "
#
# MATH LIBRARY
#MATH_MOD="intel-mkl/2017.2.174"
MATH_MOD=""
#
# LOAD MODULES
module load $COMP_MOD $MATH_MOD
module li

#
# SET NUMBER OF CORES TO USE IN PARALLEL MAKE
#
CORES_TOT=`cat /proc/cpuinfo | grep -c processor`
CORES_DIV=4
NCORES=$(( CORES_TOT / CORES_DIV))

export F77=mpiifort
export CC=mpiicc
export MPIF90=mpiifort
export FC=mpiifort
export LD=mpiifort
export IFLAGS="-I../include -I$(MKLROOT)/include/fftw"
export DFLAGS="-D__INTEL -D__OLDXML -D__DFTI -D__MPI -D__PARA -D__ELPA  -D__OPENMP -D__NON_BLOCKING_SCATTER"
export CFLAGS="-O3 -xHost -fno-alias -ansi-alias -g -mkl"
export FFLAGS="-O3 -xHost -fno-alias -ansi-alias -g -mkl"
export BLAS_LIBS_SWITCH="external"
export BLAS_LIBS=" -lmkl_intel_lp64  -lmkl_sequential -lmkl_core"
export LAPACK_LIBS_SWITCH="external"
export LAPACK_LIBS=" "
export FFT_LIBS=" "
export SCALAPACK_LIBS+"$(MKLROOT)/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group  $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_intel_thread.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_blacs_intelmpi_lp64.a -Wl,--end-group"
#export SCALAPACK_LIBS=" -lmkl_scalapack_lp64 -Wl,--start-group  -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -Wl,--end-group"

make veryclean
./configure --prefix=$INSTALL_DIR --enable-openmp --with-scalapack
cp $CURR_DIR/make.inc  $CURR_DIR/qe-$QE_VERSION
make -j $NCORES pw
#make -j $NCORES pp
#make -j $NCORES ph
#
# BUILD WEST
#
if [ "$BUILD_WEST" = true ] ; then
    echo 'BUILDING WEST VERSION $WEST_VERSION'
 git clone http://greatfire.uchicago.edu/west-public/West.git West
 cd West
 git checkout $(git for-each-ref --format="%(refname)" --sort=-taggerdate --count=1 "refs/tags/*$WEST_VERSION*")
 make
 cd ../
fi
#
# COPY BINARIES TO THE INSTALL_DIR LOCATION
if [ "$INSTALL_DIR" != "$CURR_DIR/qe-$QE_VERSION" ]; then
   make install
fi
#
# CHECK THAT THE pw EXECUTABLE EXISTS
if [ -f $INSTALL_DIR/bin/pw.x ] ; then
   if [ "$BUILD_WEST" = true ] ; then
      if [ -f $INSTALL_DIR/bin/wstat.x ] ; then
    echo "**********************************"
    echo "**  COMPILATION WAS SUCCESSFUL  **"
    echo "**********************************"
    echo " "
    echo "THE QE & WEST BINARIES INSTALL LOCATION: "
    echo " $INSTALL_DIR/bin "
    echo ""
    echo "REQUIRED MODULES:"
    echo " $COMP_MOD $MATH_MOD "
    echo ""
      else
    echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    echo "!! COMPILATION WAS UNSUCCESSFUL !!"
    echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
      fi
  else
    echo "**********************************"
    echo "**  COMPILATION WAS SUCCESSFUL  **"
    echo "**********************************"
    echo " "
    echo "THE QE BINARIES INSTALL LOCATION: "
    echo " $INSTALL_DIR/bin "
    echo ""
    echo "REQUIRED MODULES:"
    echo " $COMP_MOD $MATH_MOD "
    echo ""
  fi
else
    echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    echo "!! COMPILATION WAS UNSUCCESSFUL !!"
    echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
fi
#
# EOF